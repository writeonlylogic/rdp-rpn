import { useEffect, useState } from "react";
import { parser } from "./rpn-parser";
import Tree from "./Tree/Tree";

function App() {
  const [formulaInput, setFormulaInput] = useState("");
  const [parserResult, setParserResult] = useState([]);
  const [parserError, setParserError] = useState("");

  useEffect(() => {
    try {
      setParserError("");
      setParserResult(parser(formulaInput));
    } catch (error) {
      setParserError(error);
      setParserResult([]);
    }
  }, [formulaInput]);

  return (
    <div className="container my-5">
      <div className="row justify-content-center">
        <div className="col-12 text-center">
          <h1 className="mb-5">Reverse Polish Notation Parser</h1>
          <div className="mb-3">
            <input
              type="email"
              className="form-control text-center mb-3"
              id="formulaInput"
              aria-label="Formula"
              aria-describedby="formulaInputHelp"
              onChange={(e) => {
                setFormulaInput(e.target.value);
              }}
              value={formulaInput}
            ></input>
            <div id="formulaInputHelp" className="form-text mb-3">
              Input the formula to be parsed!
            </div>
          </div>
          {!parserError ? (
            <div className="py-5">
              { parserResult ? <Tree data={parserResult} /> : "" }
            </div>
          ) : (
            <div className="alert alert-danger my-5" role="alert">
              {parserError.toString()}
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default App;
